<?php

/**
 * @file
 * Views field handler for uc_signup order.
 */

/**
 * Field handler to show a link to an order.
 *
 * @ingroup views_field_handlers
 */
class uc_signup_handler_field_signup_order extends views_handler_field {
  /**
   * Renders the link.
   */
  function render($values) {
    return l($values->uc_signup_log_oid, 'admin/store/orders/' . $values->uc_signup_log_oid);
  }
}
