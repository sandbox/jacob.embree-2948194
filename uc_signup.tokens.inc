<?php

/**
 * @file
 * Token hooks for uc_signup.
 */

/**
 * Implements hook_token_info().
 */
function uc_signup_token_info() {
  $info['tokens']['uc_order']['signups'] = array(
    'name' => 'Signups',
    'description' => t('Finalized signups associated with the order.'),
  );
  return $info;
}

/**
 * Implements hook_tokens().
 */
function uc_signup_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  if ($type == 'uc_order' && !empty($data['uc_order'])) {
    $order = $data['uc_order'];
    $signups = uc_signup_confirmed_signups($order->order_id);
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'signups':
          $replacements[$original] = theme('uc_signup_confirmed_signups_text', array('signups' => $signups));
          break;
      }
    }
  }
  return $replacements;
}
