<?php

/**
 * @file
 * Rules integration for uc_signup.
 */

/**
 * Implements hook_rules_action_info().
 */
function uc_signup_rules_action_info() {
  $order_arg = array(
    'order' => array(
      'type' => 'uc_order',
      'label' => t('Order'),
    ),
  );
  $actions['uc_signup_cancel_signups'] = array(
    'label' => t('Cancel temporary signups for the current user'),
    'group' => t('uc_signup'),
    'callbacks' => array(
      'execute' => 'uc_signup_cancel_temporary_signups',
    ),
    'parameter' => $order_arg,
  );
  $actions['uc_signup_mark_paid'] = array(
    'label' => t('Mark temporary signups as paid'),
    'group' => t('uc_signup'),
    'callbacks' => array(
      'execute' => 'uc_signup_mark_paid',
    ),
    'parameter' => $order_arg,
  );
  return $actions;
}
