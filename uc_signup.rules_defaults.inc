<?php

/**
 * @file
 * Default rules configurations for uc_signup.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function uc_signup_default_rules_configuration() {
  $configs = array();

  $rule = rules_reaction_rule();
  $rule->label = t('Change temporary signups to paid signups upon checkout when a payment clears the order balance.');
  $rule->active = TRUE;
  $rule->event('uc_checkout_complete')
    ->action('uc_signup_mark_paid', array(
      'order:select' => 'order',
    ));
  $configs['uc_signup_mark_paid'] = $rule;

  $rule = rules_reaction_rule();
  $rule->label = t('Cancel temporary signups created by uc_signup when an order is cancelled.');
  $rule->active = TRUE;
  $rule->event('uc_checkout_complete')
    ->condition('uc_order_condition_order_state', array(
      'order:select' => 'order',
      'order_state' => 'canceled',
    ))
    ->action('uc_signup_cancel_signups', array(
      'order:select' => 'order',
    ));
  $configs['uc_signup_cancel_signups'] = $rule;

  return $configs;
}
